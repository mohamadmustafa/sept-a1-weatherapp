package com.example.teamredrmit.weatherapp;

import com.example.teamredrmit.weatherapp.Controller.WeatherService;
import com.example.teamredrmit.weatherapp.Model.Forecast;
import com.example.teamredrmit.weatherapp.Utils.Coordinate;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.MoreExecutors;

import org.junit.Before;
import org.junit.Test;

import java.net.MalformedURLException;
import java.net.URL;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class ForecastTest {

    URL path;
    Coordinate location;
    WeatherService service;

    @Before
    public void initialize() {
        service = new WeatherService(MoreExecutors.newDirectExecutorService());
        location = new Coordinate(144.9632, -37.8104);

        try {
            path = new URL("https://api.forecast.io/forecast/2ae60090fc163cc3b9b550de4fe13f7d/" + location.latitude + "," + location.longitude);
        } catch (MalformedURLException e) {
            fail();
        }
    }

    @Test
    public void forecastRetrieval() throws Exception {
        final ListenableFuture worker = service.getCurrentForecast(path);
        Futures.addCallback(worker, new FutureCallback<Forecast>() {
            public void onSuccess(Forecast forecast) {
                assertTrue(location.longitude.equals(forecast.getLongitude()) &&
                        location.latitude.equals(forecast.getLatitude()));
            }

            public void onFailure(Throwable thrown) {
                System.out.println(thrown.getMessage());
                fail();
            }
        });
    }
}
