package com.example.teamredrmit.weatherapp;

import com.example.teamredrmit.weatherapp.Model.ForecastDaily;

import org.junit.Test;
import static org.junit.Assert.*;

public class ForecastDailyTest {

    //Following code tests all the getters and setters of the ForecastDaily class.
    ForecastDaily fd = new ForecastDaily();
    String a = "Hello";

    @Test
    public void timezoneTest() {
        fd.setSummary(a);
        assertEquals(a, fd.getSummary());
    }

    @Test
    public void iconTest() {
        fd.setIcon(a);
        assertEquals(a, fd.getIcon());
    }
}
