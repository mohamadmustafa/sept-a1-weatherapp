package com.example.teamredrmit.weatherapp.View.Fragments;

import android.app.Fragment;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.db.chart.Tools;
import com.db.chart.listener.OnEntryClickListener;
import com.db.chart.model.ChartEntry;
import com.db.chart.model.LineSet;
import com.db.chart.view.AxisController;
import com.db.chart.view.LineChartView;
import com.example.teamredrmit.weatherapp.Model.Observation;
import com.example.teamredrmit.weatherapp.Model.Station;
import com.example.teamredrmit.weatherapp.R;
import com.example.teamredrmit.weatherapp.View.WeatherTables;
import com.google.common.collect.Iterables;
import com.google.common.primitives.Floats;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import it.sephiroth.android.library.tooltip.Tooltip;

public class GraphFragment extends Fragment {

    private static final String TAG = "GraphFragment";

    private List<Observation> readings = new ArrayList<>();
    private Point point;
    private LineChartView chart;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.graphfragment, container, false);

        chart = (LineChartView) view.findViewById(R.id.lineChart);
        chart.setBorderSpacing(Tools.fromDpToPx(15))
                .setAxisBorderValues(0, 50)
                .setYLabels(AxisController.LabelPosition.NONE)
                .setXLabels(AxisController.LabelPosition.NONE)
                .setLabelsColor(Color.parseColor("#6a84c3"))
                .setXAxis(false)
                .setYAxis(false);

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        chart.reset();
        readings.clear();

        chart = null;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    public void setObservations(List<Observation> results) {
        readings.addAll(results);
    }

    public void setStation(Station station) {
        View view = getView();
        if (view != null) {
            TextView title = (TextView) view.findViewById(R.id.table_title);
            title.setText(station.getName());
        }
    }

    public void toggleGraph(final WeatherTables.MeasurementType measurement, int nodeInterval) {
        final View view = getView();
        if (view != null) {
            TextView type = (TextView) view.findViewById(R.id.table_type);
            switch (measurement) {
                case TEMPERATURE:
                    type.setText(R.string.graph_temperature);
                    break;
                case WIND:
                    type.setText(R.string.graph_wind);
                    break;
                case RAIN:
                    type.setText(R.string.graph_rain);
                    break;
            }

        } else return;

        DateTimeFormatter format = DateTimeFormat
                .forPattern("h:mma");

        List<String> labels = new ArrayList<>();
        List<Float>  values = new ArrayList<>();

        LocalDate today = new LocalDate().minusDays(1);
        Float reading = 0.0f;

        for (Observation o : readings) {

            switch (measurement) {
                case TEMPERATURE:
                    reading = o.getAirTemp();
                    break;
                case WIND:
                    try {
                        if(o.getWindSpdKmh() != null){
                            reading = Float.parseFloat(o.getWindSpdKmh().toString());
                        }
                    } catch (NumberFormatException e) {
                        reading = 0.0f;
                    }
                    break;
                case RAIN:
                    try {
                        reading = Float.parseFloat(o.getRainTrace());
                    } catch (NumberFormatException e) {
                        reading = 0.0f;
                    }
                    break;
            }

            DateTime time = o.getLocalDateTimeFull();
            LocalDate date = time.toLocalDate();
            if (date.isEqual(today) &&
                    (time.getMinuteOfHour() == 0) &&
                    (time.getHourOfDay() % nodeInterval == 0)) {
                String label = time.toString(format);
                labels.add(label);
                values.add(reading);
            }
        }

        final LineSet lineSet = new LineSet(
                Iterables.toArray(labels, String.class),
                Floats.toArray(values));

        drawGraph(lineSet);

        if(lineSet.getEntries().size() > 1){

            chart.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    int x = (int)event.getRawX();
                    int y = (int)event.getRawY();

                    point = new Point(x,y);

                return false;
            }
        });

            chart.setOnEntryClickListener(new OnEntryClickListener() {
                @Override
                public void onClick(int setIndex, int entryIndex, Rect entryRect) {
                    ChartEntry entry = lineSet.getEntry(entryIndex);

                    if (entry != null) {
                        String label = entry.getLabel();
                        String value = "";
                        switch (measurement) {
                            case RAIN:
                                value = String.format(Locale.getDefault(),
                                        "%.1f mm", entry.getValue());
                                break;
                            case WIND:
                                value = String.format(Locale.getDefault(),
                                        "%.0f km/h", entry.getValue());
                                break;
                            case TEMPERATURE:
                                value = String.format(Locale.getDefault(),
                                        "%.1f °C", entry.getValue());
                                break;
                        }

                        showToolTip(String.format(Locale.getDefault(),
                                "%s | %s", label, value));
                    }
                }
            });

        }
    }

    private void drawGraph(LineSet set) {
        // Clear previous dataset, if any
        chart.reset();

        if (set.size() > 1) {
            set.setColor(Color.parseColor("#FFFFFF"))
                    .setDotsRadius(10)
                    .setDotsColor(Color.parseColor("#F4A62A"))
                    .setDotsStrokeThickness(3)
                    .setDotsStrokeColor(Color.parseColor("#FFFFFF"))
                    .setThickness(3);

            chart.addData(set);
            colourEntries(chart, set);

            chart.setLabelsColor(Color.parseColor("#FFFFFF"));
            chart.setStep(5);
            chart.setYLabels(AxisController.LabelPosition.OUTSIDE);

            chart.show();
        }
    }

    private void colourEntries(LineChartView chart, LineSet values) {
        List<ChartEntry> entries = values.getEntries();
        if (!entries.isEmpty()) {
            ChartEntry max = entries.get(0);
            ChartEntry min = entries.get(0);

            for (int i = 0; i < entries.size(); i++) {
                ChartEntry entry = entries.get(i);
                if (entry.getValue() > max.getValue()) {
                    max = entry;
                } else if (entry.getValue() < min.getValue()) {
                    min = entry;
                }
            }

            min.setColor(Color.parseColor("#3D566E"));
            max.setColor(Color.parseColor("#EA6052"));

            int minAxis = (int) Math.floor(min.getValue());
            int maxAxis = (int) Math.ceil(max.getValue());

            chart.setAxisBorderValues(minAxis, maxAxis);
        }
    }

    private void showToolTip(String text) {
        Tooltip.make(getActivity(),
                new Tooltip.Builder(101)
                        .anchor(point, Tooltip.Gravity.TOP)
                        .closePolicy(new Tooltip.ClosePolicy()
                                .insidePolicy(true, false)
                                .outsidePolicy(true, false), 4000)
                        .activateDelay(900)
                        .showDelay(0)
                        .text(text)
                        .maxWidth(600)
                        .withArrow(true)
                        .withOverlay(true).build()
        ).show();
        Log.i(TAG, "Tooltip activated - " + text);
    }
}
