package com.example.teamredrmit.weatherapp.Utils;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.lang.reflect.Type;

public class DateTimeDeserializer implements JsonDeserializer<DateTime> {
    // JodaTime format for the local_date_time_full field parsing
    private DateTimeFormatter BOMFormat = DateTimeFormat.forPattern("yyyyMMddHHmmss");

    @Override
    public DateTime deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
        throws JsonParseException {
        String dt = context.deserialize(json, String.class);
        return BOMFormat.parseDateTime(dt);
    }
}
