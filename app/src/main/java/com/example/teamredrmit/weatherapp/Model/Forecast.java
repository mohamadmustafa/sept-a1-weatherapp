package com.example.teamredrmit.weatherapp.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/* Data for all the Forecast structures were originally
 generated at http://www.jsonschema2pojo.org/ and
 partially edited afterwards */

public class Forecast {

    @SerializedName("latitude")
    @Expose
    private Double latitude;
    @SerializedName("longitude")
    @Expose
    private Double longitude;
    @SerializedName("timezone")
    @Expose
    private String timezone;
    @SerializedName("offset")
    @Expose
    private Double offset;

    @SerializedName("currently")
    @Expose
    private ForecastCurrently currently;

    @SerializedName("daily")
    @Expose
    private ForecastDaily daily;

    public Forecast() {
    }

    public Forecast(Double latitude, Double longitude, String timezone, Double offset,ForecastCurrently currently, ForecastDaily daily) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.timezone = timezone;
        this.offset = offset;
        this.daily = daily;
    }

    /**
     *
     * @return
     *     The latitude
     */
    public Double getLatitude() {
        return latitude;
    }

    /**
     *
     * @param latitude
     *     The latitude
     */
    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    /**
     *
     * @return
     *     The longitude
     */
    public Double getLongitude() {
        return longitude;
    }

    /**
     *
     * @param longitude
     *     The longitude
     */
    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    /**
     *
     * @return
     *     The timezone
     */
    public String getTimezone() {
        return timezone;
    }

    /**
     *
     * @param timezone
     *     The timezone
     */
    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    /**
     *
     * @return
     *     The offset
     */
    public Double getOffset() {
        return offset;
    }

    /**
     *
     * @param offset
     *     The offset
     */
    public void setOffset(Double offset) {
        this.offset = offset;
    }

    /**
     *
     * @return
     *     The daily
     */
    public ForecastCurrently getCurrently() {
        return currently;
    }

    /**
     *
     * @param currently
     *     The currently
     */
    public void setCurrently(ForecastCurrently currently) {
        this.currently = currently;
    }

    /**
     *
     * @return
     *     The daily
     */
    public ForecastDaily getDaily() {
        return daily;
    }

    /**
     *
     * @param daily
     *     The daily
     */
    public void setDaily(ForecastDaily daily) {
        this.daily = daily;
    }

}
