package com.example.teamredrmit.weatherapp.View;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.teamredrmit.weatherapp.R;

public class StationViewHolder extends RecyclerView.ViewHolder {

    TextView name;
    TextView currentTemp;

    ImageView weatherImage;
    RelativeLayout cardContainer;

    public View stationView;
    public StationViewHolder(View itemView) {
        super(itemView);

        name = (TextView)itemView.findViewById(R.id.name);
        currentTemp = (TextView)itemView.findViewById(R.id.currentTemp);

        weatherImage = (ImageView)itemView.findViewById(R.id.weatherImage);
        cardContainer = (RelativeLayout)itemView.findViewById(R.id.cardWrap);

        stationView = itemView;
    }
}
