package com.example.teamredrmit.weatherapp.Model;

import android.os.Parcel;
import android.os.Parcelable;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Objects;

public class Station implements Parcelable {

    public static final Parcelable.Creator<Station> CREATOR = new Parcelable.Creator<Station>() {
        public Station createFromParcel(Parcel in) {
            return new Station(in);
        }

        public Station[] newArray(int size) {
            return new Station[size];
        }
    };

    private String id;
    private String name;
    private String location;
    private URL url;

    public Station() {
        // Empty default constructor required by Firebase
    }

    public Station(String id, String name, String location, URL url) {
        this.id = id;
        this.name = name;
        this.location = location;
        this.url = url;
    }

    /* Constructor for Android Parcelable interface. For passing
    Station through Intents */

    public Station(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
        this.location = in.readString();

        try {
            this.url = new URL(in.readString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    public String getId() { return id; }
    public String getName() { return name; }
    public String getLocation() { return location; }
    public URL getUrl() { return url; }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(location);
        dest.writeString(url.toString());
    }

    @Override
    public int describeContents() {
        return hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Station station = (Station) o;
        return Objects.equals(id, station.id) &&
                Objects.equals(name, station.name) &&
                Objects.equals(location, station.location) &&
                Objects.equals(url, station.url);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, location, url);
    }
}
