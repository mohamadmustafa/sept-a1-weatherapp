package com.example.teamredrmit.weatherapp.View;

import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.teamredrmit.weatherapp.Model.Forecast;
import com.example.teamredrmit.weatherapp.Model.ForecastDaily;
import com.example.teamredrmit.weatherapp.Model.ForecastData;
import com.example.teamredrmit.weatherapp.R;
import com.example.teamredrmit.weatherapp.Utils.Forecaster;
import com.example.teamredrmit.weatherapp.Utils.Validator;
import com.google.common.base.Optional;
import com.google.common.collect.Iterables;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class ForecastAdapter extends RecyclerView.Adapter<ForecastHolder> {

    private List<ForecastData> forecasts;

    private Forecaster icons;
    private Handler handler = new Handler();

    public ForecastAdapter(Forecaster f, ListenableFuture forecast) {
        icons = f;
        forecasts = new ArrayList<>();

        Futures.addCallback(forecast, new FutureCallback<Forecast>() {
            public void onSuccess(final Forecast response) {
                handler.post(new Runnable() {
                    public void run() {
                        if (response != null) {
                            ForecastDaily daily = response.getDaily();
                            forecasts.addAll(daily.getData());
                            notifyDataSetChanged();
                        }
                    }
                });
            }

            public void onFailure(Throwable thrown) {
                thrown.printStackTrace();
            }
        });
    }

    @Override
    public void onBindViewHolder(ForecastHolder forecastHolder, int i) {
        ForecastData data = forecasts.get(i);

        forecastHolder.setDatetime(Validator.getWeekDay(data.getTime()));
        forecastHolder.setWeatherImage(icons.getWeatherDrawable(data.getIcon()));
        forecastHolder.setMaxTemp(data.getApparentTemperatureMax());
        forecastHolder.setMinTemp(data.getApparentTemperatureMin());
    }

    @Override
    public ForecastHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.forecasts, viewGroup, false);
        return new ForecastHolder(view);
    }

    @Override
    public int getItemCount() {
        return forecasts.size();
    }
}
