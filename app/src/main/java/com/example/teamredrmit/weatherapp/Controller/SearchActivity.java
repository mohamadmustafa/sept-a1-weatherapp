package com.example.teamredrmit.weatherapp.Controller;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.teamredrmit.weatherapp.Model.Station;
import com.example.teamredrmit.weatherapp.R;
import com.example.teamredrmit.weatherapp.View.SearchViewHolder;
import com.firebase.client.Firebase;
import com.firebase.client.Query;
import com.firebase.ui.FirebaseRecyclerAdapter;


public class SearchActivity extends AppCompatActivity {
    ProgressDialog pDialog;
    private static final String TAG = "Search Activity";
    private boolean stationsFound = false;
    private int stationCount = 0;
    private TextView resultStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search);
        Intent myIntent = getIntent(); // gets the previously created intent
        String searchString = myIntent.getStringExtra("inputText");
        doSearch(searchString);
    }

    //back home page when back button pressed.
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        startActivity(new Intent(this, MainActivity.class));

    }

    private void doSearch(String inputText) {

        Log.i(TAG, "Starting Search" );

        //noResult = (TextView ) findViewById(R.id.tvNoResults);
        RecyclerView searchRv = (RecyclerView) findViewById(R.id.searchResultRecyclerView);
        searchRv.setHasFixedSize(false);
        searchRv.setLayoutManager(new LinearLayoutManager(this));

        Firebase.setAndroidContext(this);
        String firebaseURL = "https://crackling-fire-9834.firebaseio.com/stations";

        Firebase stations = new Firebase(firebaseURL);
        SearchStation(stations, searchRv, inputText);
    }

    private void SearchStation( Firebase station, RecyclerView rv, String inputText){
        //filter the input text
        inputText = toDisplayCase(inputText).trim();
        Query fbQuery = station.orderByChild("name").startAt(inputText).endAt(inputText+"\uf8ff").limitToFirst(15);
        Log.d(TAG,"Inside SearchStation");

        FirebaseRecyclerAdapter searchAdapter;


        pDialog = new ProgressDialog(this);
        pDialog.setTitle("Weather App");
        pDialog.setMessage("Retrieving Stations...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(true);
        //pDialog.show();

        searchAdapter = new FirebaseRecyclerAdapter<Station, SearchViewHolder>
                (Station.class, R.layout.search_result_view, SearchViewHolder.class, fbQuery) {

            TextView noResult = (TextView ) findViewById(R.id.tvNoResults);

            @Override
            protected void populateViewHolder(final SearchViewHolder svh, final Station station, int position) {
                Log.d(TAG, "Populate Results: "+ station.getId());
                noResult.setVisibility(View.GONE);
                if(!station.getName().equals(" ") ){
                    stationCount++;
                    Log.i(TAG, "Search Returned " + station.getName());
                    if (pDialog != null && pDialog.isShowing()) {
                         pDialog.dismiss();
                    }
                }
                svh.getCityText().setText(station.getName());
                svh.getStateText().setText(station.getLocation());
                final ImageButton favBtn = svh.getFavButton();

                /*if(Favourites.isItFavouriteStation(station.getId())){
                    favBtn.setImageResource(R.drawable.star_fav);
                }
                else{
                    favBtn.setVisibility(View.VISIBLE);
                }*/

                favBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        addFavourite(station);
                    }
                });
            }
        };

        rv.setAdapter(searchAdapter);
    }

    private void addFavourite(Station s){
        finish();
        Intent add = new Intent(this, MainActivity.class);
        add.putExtra("station", s);
        startActivity(add);
    }

    // To  format input string
    // Ref http://stackoverflow.com/questions/1086123/string-conversion-to-title-case
    public static String toDisplayCase(String s) {

        final String ACTIONABLE_DELIMITERS = " '-/"; // these cause the character following
        // to be capitalized

        StringBuilder sb = new StringBuilder();
        boolean capNext = true;

        for (char c : s.toCharArray()) {
            c = (capNext)
                    ? Character.toUpperCase(c)
                    : Character.toLowerCase(c);
            sb.append(c);
            capNext = (ACTIONABLE_DELIMITERS.indexOf((int) c) >= 0); // explicit cast not needed
        }
        return sb.toString();
    }
}