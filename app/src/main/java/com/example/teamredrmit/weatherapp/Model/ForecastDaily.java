package com.example.teamredrmit.weatherapp.Model;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ForecastDaily {
    @SerializedName("summary")
    @Expose
    private String summary;
    @SerializedName("icon")
    @Expose
    private String icon;
    @SerializedName("data")
    @Expose
    private List<ForecastData> data = new ArrayList<ForecastData>();

    public ForecastDaily() {
    }

    public ForecastDaily(String summary, String icon, List<ForecastData> data) {
        this.summary = summary;
        this.icon = icon;
        this.data = data;
    }

    /**
     *
     * @return
     *     The summary
     */
    public String getSummary() {
        return summary;
    }

    /**
     *
     * @param summary
     *     The summary
     */
    public void setSummary(String summary) {
        this.summary = summary;
    }

    /**
     *
     * @return
     *     The icon
     */
    public String getIcon() {
        return icon;
    }

    /**
     *
     * @param icon
     *     The icon
     */
    public void setIcon(String icon) {
        this.icon = icon;
    }

    /**
     *
     * @return
     *     The data
     */
    public List<ForecastData> getData() {
        return data;
    }

    /**
     *
     * @param data
     *     The data
     */
    public void setData(List<ForecastData> data) {
        this.data = data;
    }
}
