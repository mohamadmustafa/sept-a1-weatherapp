package com.example.teamredrmit.weatherapp.Model;

import com.example.teamredrmit.weatherapp.Model.Station;

public interface StationListener {
    void onStationAdded(Station s);
}