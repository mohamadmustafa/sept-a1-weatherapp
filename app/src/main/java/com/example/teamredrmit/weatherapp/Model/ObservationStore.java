package com.example.teamredrmit.weatherapp.Model;

import com.example.teamredrmit.weatherapp.Controller.ObservationOrdering;
import com.example.teamredrmit.weatherapp.Controller.WeatherService;
import com.example.teamredrmit.weatherapp.Utils.BOMResponse;
import com.google.common.base.Optional;
import com.google.common.collect.Iterables;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;

public class ObservationStore {

    private static class StoreHolder {
        static final ObservationStore INSTANCE = new ObservationStore();
    }

    public static ObservationStore getInstance() {
        return StoreHolder.INSTANCE;
    }

    private List<ObservationListener> listeners;
    private Map<Station, List<Observation>> store;

    // Handles the querying to BOM
    private WeatherService service = new WeatherService(Executors.newFixedThreadPool(5));
    private ObservationOrdering comparator = new ObservationOrdering();

    private ObservationStore() {
        listeners = new ArrayList<>();
        store = new ConcurrentHashMap<>();
    }

    public synchronized ObservationListener addObservationListener(ObservationListener listener) {
        if (!listeners.contains(listener)) {
            listeners.add(listener);
        }

        return listener;
    }

    public synchronized void removeObservationListener(ObservationListener listener) {
        listeners.remove(listener);
    }

    public synchronized void observeStation(Station s) {
        if (!store.containsKey(s)) {
            store.put(s, new ArrayList<Observation>());
            updateStationObservations(s);
        }
    }

    public synchronized void removeStation(Station s) {
        store.remove(s);
    }

    public synchronized void updateAllObservations() {
        for (Station s : store.keySet()) {
            updateStationObservations(s);
        }
    }

    public synchronized void updateStationObservations(final Station s) {
        ListenableFuture worker = service.getCurrentMeasurements(s.getUrl());
        Futures.addCallback(worker, new FutureCallback<BOMResponse>() {
            public void onSuccess(BOMResponse response) {
                List<Observation> readings = response.getObservations().getData();

                /* Loop through all the results returned by BOM and only add them to the Station's
                 * observations list if it isn't already present. This is to prevent duplicates from being
                 * added every time BOM is queried by WeatherService for the latest weather observations. */

                boolean hasChanged = false;
                List<Observation> current = store.get(s);
                for (Observation o : readings) {
                    if (!current.contains(o)) {
                        current.add(o);
                        // Sort the collection using the DateTime instance based on localDateTimeFull
                        Collections.sort(current, comparator);
                        hasChanged = true;
                    }
                }

                /* BOM always returns all the results from within the last 72 hours. Therefore,
                 * when we periodically check for additional observations explicitly check whether or not
                 * an Observation was received that wasn't already tracked. */

                if (hasChanged) {
                    notifyAllListeners(Iterables.getLast(current, null), s);
                }
            }

            public void onFailure(Throwable thrown) {
                System.out.println("OnFailure Callback on Future Ran");
            }
        });
    }

    public synchronized Optional<List<Observation>> getAllObservations(Station s) {
        return Optional.fromNullable(store.get(s));
    }

    public synchronized Optional<Observation> getLastObservation(Station s) {
        List<Observation> observations = store.get(s);
        return Optional.fromNullable(Iterables.getLast(observations, null));
    }

    public synchronized  WeatherService getService() {
        return service;
    }

    /* ObservationStore is a Singleton to facilitate the access of the Station's observations
     * across separate Android activities, however, this complicates unit testing and the ability to
     * substitute a mock WeatherService to return test values. By allowing the setting of the service provider
     * this provides some flexibility with testing. */
    public void setDataProvider(WeatherService service) {
        if (service != null) {
            this.service = service;
        }
    }

    private synchronized void notifyAllListeners(Observation o, Station s) {
        for (ObservationListener listener : listeners) {
            listener.onObservationAdded(o, s);
        }
    }
}
