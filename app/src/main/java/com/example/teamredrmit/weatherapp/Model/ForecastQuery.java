package com.example.teamredrmit.weatherapp.Model;

import android.util.Log;

import com.google.common.base.Optional;

import java.net.MalformedURLException;
import java.net.URL;

public class ForecastQuery {

    public static Optional<URL> getForecastURL(double latitude, double longitude) {
        try {
            URL path = new URL("https://api.forecast.io/forecast/cabd3c93bad55030c01d7b38012e0d33/" +
                    latitude + "," + longitude + "?units=auto&exclude=[minutely,hourly,alerts,flags]");
            Log.i("ForeCastQuery",path.toString());
            return Optional.of(path);
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return Optional.absent();
        }
    }
}