package com.example.teamredrmit.weatherapp;

import android.test.suitebuilder.annotation.SmallTest;

import com.example.teamredrmit.weatherapp.Model.ObservationStore;
import com.example.teamredrmit.weatherapp.Model.Station;

import junit.framework.TestCase;

import java.net.URL;

public class ObservationStoreTest extends TestCase {
    private ObservationStore s;

    public void runTest() {
        testObservationListeners();
    }

    protected void setUp() throws Exception {
        s = ObservationStore.getInstance();
        //s.setProvider(new MockService());
        s.observeStation(new Station("1", "Test Station", "Test", new URL("http://www.google.com")));
    }

    @SmallTest
    public void testObservationListeners() {

    }
}
