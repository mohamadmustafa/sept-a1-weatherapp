This repository is a fork from my other Bitbucket account which I've used to contibute in developing this project.

# 1.0 - Installation #

Installation would usually be done via the Google Play Store after the app has been published. However, as required, an installation guide will be given for the prototype state of the app as follows:

## 1.1 - Sideload Signed APK ##

**Step 1:**
On your Android device, enable Unknown sources under the Security category in Settings. This option allows Android APKs to be installed to the phone from sources other than the Google Play store. This step is required for installation to occur correctly.

**Step 2:**
Download the Red Weather application APK from the Bitbucket repository by navigating to the URL below in the device's web browser. [APK Download Link](http://teamredrmit.bitbucket.org/redweather.apk)

**Step 3:**
Run the downloaded application APK, which will launch Android's APK installation utility that will install Red Weather to the mobile device.

**Step 4:**
See: [2.0 - Using the application](teamredrmit.bitbucket.org/), for further instructions after the app has been loaded onto the phone.

## 1.2 - Building Red Weather From Source ##

**Note:**
A basic level of technical expertise and familiarity with both Git and Android Studio will be required to install the prototype version.

**Step 1:**
Download and install Git. Please use the following guide if Git is not yet installed: [Link](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

**Step 2:**
Open the terminal in your operating system and clone the project into a local repository using the following command: "git clone git@bitbucket.org:TeamRedRMIT/weatherapp.git"

**Step 3:**
Download and install Android Studio. Please use the following guide if Android Studio is not yet installed: [Link](http://developer.android.com/sdk/installing/index.html)

**Step 4:**
After installation, open Android Studio and choose "Open an existing Android Studio project" from the Quick Start menu.

**Step 5:**
Navigate to the folder that you have cloned from Git in Step 2. Choose/Select this folder. Agree and accept any prompts that Android Studio may ask you.

**Step 6:**
Locate 'Run' in the menu bar and select "Run 'app'" as your option. Either use the default emulator or plug in your own Android phone to load and run the app.

**Step 7:**
See: [2.0 - Using the application](teamredrmit.bitbucket.org/), for further instructions after the app has been loaded onto a phone/emulator.